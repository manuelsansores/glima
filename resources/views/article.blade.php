@extends('templates.principal')
@section('content')
    <br>
    <div class="container d-flex justify-content-end" >
        <button class="btn btn-primary" id="btn-new">Nuevo</button>
    </div>
    <br>
    <div class="container" id="article-content">
        <form action="#"  id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">
            <input type="hidden" id="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="article_id">
            <div class="form-group">
                <label for="exampleInputEmail1">Artículo</label>
                <input type="text" name="name_article" id="name_article" class="form-control">
                <div id="error-article"> </div>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">pu</label>
                <input type="text" name="pu" id="pu" class="form-control">
                <div id="error-pu"> </div>
            </div>
            <div class="float-right">
                <button class="btn btn-success" id="btn-save">Guardar</button>
                <button class="btn btn-success" id="btn-edit">Editar</button>
                <button class="btn btn-danger" id="btn-cancel">Cancelar</button>
            </div>

        </form>
        <br><br><br>
    </div>


   <div class="container">
       <table id="article" class="visible responsive nowrap table table-striped" style="width:100%">
           <thead>
           <tr>
               <th>id</th>
               <th>Artículo</th>
               <th>precio Unitario</th>
               <th></th>

           </tr>
           </thead>
           <tfoot>
           <tr>
               <th>id</th>
               <th>Artículo</th>
               <th>precio Unitario</th>
               <th></th>

           </tr>
           </tfoot>
       </table>
   </div>

@endsection

@section('script')
<script src="/js/article.js"></script>
@endsection