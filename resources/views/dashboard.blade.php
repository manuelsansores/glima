@extends('templates/default')
@section('title','dashboard')

@section('content')
    <!-- Home -->

    <div class="home">

        <!-- Home Slider -->

        <div class="home_slider_container">
            <div class="owl-carousel owl-theme home_slider">

                <!-- Home Slider Item -->
                <div class="owl-item">
                    <div class="home_slider_background">
                        <img src="http://glima.com.mx/img/slider1.jpg" alt="">
                    </div>
                    <div class="home_slider_content text-center">

                        <h1>Lorem ipsum dolor sit amet</h1>
                        <div class="button home_slider_button"><a href="#">Contacto</a></div>
                    </div>
                </div>

                <!-- Home Slider Item -->
                <div class="owl-item">
                    <div class="home_slider_background">
                        <img src="http://glima.com.mx/img/slider2.jpg" alt="">
                    </div>
                    <div class="home_slider_content text-center">

                        <h1>Lorem ipsum dolor sit amet</h1>
                        <div class="button home_slider_button"><a href="#">Contacto</a></div>
                    </div>
                </div>

                <!-- Home Slider Item -->
                <div class="owl-item">
                    <div class="home_slider_background">
                        <img src="http://glima.com.mx/img/slider3.jpg" alt="">
                    </div>
                    <div class="home_slider_content text-center">

                        <h1>ContactoLorem ipsum dolor sit amet</h1>
                        <div class="button home_slider_button"><a href="#">Contacto</a></div>
                    </div>
                </div>

            </div>
            <div class="home_slider_nav d-flex flex-column align-items-center justify-content-center">
                <img src="/img/arrow_r.png" alt="">
            </div>

        </div>
    </div>

    <!-- Intro -->

    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section_title_container">

                        <div class="section_title"><h2>BIENVENIDO AL MUNDO DE LAS LLANTAS !!</h2></div>
                        <div class="section_subtitle">God loves us all</div>
                    </div>
                </div>
            </div>
            <div class="row intro_content">

                <!-- Intro Text -->
                <div class="col-lg-6">
                    <div class="intro_text">
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates reiciendis iure harum expedita cum vero amet veritatis eligendi. Iure quaerat ex blanditiis commodi eos quia maxime mollitia culpa. Nulla, exercitationem!
                        </p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis harum consequuntur, laudantium magnam eaque delectus incidunt nemo. Delectus vel maxime incidunt, enim rerum, possimus repellat iste doloribus consequuntur, corrupti veritatis.</p>
                        <div class="button intro_button"><a href="#">Read More</a></div>
                    </div>
                </div>

                <!-- Intro Image -->

                <div class="col-lg-6">
                    <div class="intro_image"><img src="/img/llantas.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </div>

    <!-- products -->
    <div class="causes">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section_title_container">

                        <div class="section_title"><h2>Productos</h2></div>
                        <div class="section_subtitle">Lorem ipsum</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="causes_slider_container">

                        <!-- Causes Slider -->
                        <div class="owl-carousel owl-theme causes_slider">

                            <!-- Causes Slider Item -->
                            <div class="owl-item text-center causes_item">
                                <div class="causes_item_image"><img src="/img/producto/1.png" alt=""></div>
                                <div class="causes_item_title">Lorem ipsum </div>
                                <div class="causes_item_text">
                                    <p>Praesent malesuada congue magna at finibus. In hac habitasse platea dictumst.</p>
                                </div>
                            </div>

                            <!-- Causes Slider Item -->
                            <div class="owl-item text-center causes_item">
                                <div class="causes_item_image"><img src="/img/producto/2.png" alt=""></div>
                                <div class="causes_item_title">Lorem ipsum </div>
                                <div class="causes_item_text">
                                    <p>Praesent malesuada congue magna at finibus. In hac habitasse platea dictumst.</p>
                                </div>
                            </div>

                            <!-- Causes Slider Item -->
                            <div class="owl-item text-center causes_item">
                                <div class="causes_item_image"><img src="/img/producto/3.png" alt=""></div>
                                <div class="causes_item_title">Community Food</div>
                                <div class="causes_item_text">
                                    <p>Praesent malesuada congue magna at finibus. In hac habitasse platea dictumst.</p>
                                </div>
                            </div>

                            <!-- Causes Slider Item -->
                            <div class="owl-item text-center causes_item">
                                <div class="causes_item_image"><img src="/img/producto/4.png" alt=""></div>
                                <div class="causes_item_title">Lorem ipsum</div>
                                <div class="causes_item_text">
                                    <p>Praesent malesuada congue magna at finibus. In hac habitasse platea dictumst.</p>
                                </div>
                            </div>
                            <div class="owl-item text-center causes_item">
                                <div class="causes_item_image"><img src="/img/producto/5.png" alt=""></div>
                                <div class="causes_item_title">Lorem ipsum</div>
                                <div class="causes_item_text">
                                    <p>Praesent malesuada congue magna at finibus. In hac habitasse platea dictumst.</p>
                                </div>
                            </div>

                        </div>

                        <div class="causes_slider_nav causes_slider_prev trans_200 d-flex flex-column align-items-center justify-content-center"><img src="/img/arrow_l.png" alt=""></div>
                        <div class="causes_slider_nav causes_slider_next trans_200 d-flex flex-column align-items-center justify-content-center"><img src="/img/arrow_r.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact Form -->
    <br>
    <div class="contact_form_section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="contact_title">Contacto</div>
                    <form action="#" class="contact_form">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="contact_input contacts_input_name" placeholder="Your Name" required="required">
                            </div>
                            <div class="col-md-6">
                                <input type="email" class="contact_input contact_input_email" placeholder="Your E-mail" required="required">
                            </div>
                        </div>
                        <textarea id="contact_text_area" placeholder="Your Message" class="contact_text"></textarea>
                        <button class="contact_button trans_200">Submit</button>
                    </form>
                </div>
            </div>
            <div class="row">

            </div>
        </div>
    </div>
@endsection