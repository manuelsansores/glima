@extends('templates.principal')
@section('content')
    <br>
    <div class="container d-flex justify-content-end" >
        <button class="btn btn-primary" id="btn-new">Nuevo</button>
    </div>
    <br>
    <div class="container" id="client-content">
        <form action="#"  id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">
            <input type="hidden" id="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="client_id">
            <div class="form-group">
                <label for="exampleInputEmail1">Cliente</label>
                <input type="text" name="client" id="name_client" class="form-control">
                <div id="error-article"> </div>
            </div>
            <div class="float-right">
                <button class="btn btn-success" id="btn-save">Guardar</button>
                <button class="btn btn-success" id="btn-edit">Editar</button>
                <button class="btn btn-danger" id="btn-cancel">Cancelar</button>
            </div>
        </form>
        <br><br><br>
    </div>
    <div class="container">
        <table id="cliente" class="visible responsive nowrap table table-striped" style="width:100%">
            <thead>
            <tr>
                <th>id</th>
                <th>Cliente</th>
                <th></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>id</th>
                <th>Cliente</th>
                <th></th>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection
@section('script')
<script src="/js/client.js"></script>
@endsection