<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Demo project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/styles/bootstrap4/bootstrap.min.css">
    <link href="/js/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="/css/styles/main_styles.css">
    <link rel="stylesheet" type="text/css" href="/css/styles/responsive.css">
    <link rel="stylesheet" type="text/css" href="/css/styles/contact.css">
</head>
<body>

<div class="super_container">

    <!-- Header -->

    <header class="header">

        <!-- Top Bar -->

        <div class="top_bar">
            <div class="container">
                <div class="row">
                    <div class="col top_bar_content d-flex flex-row align-items-center justify-content-start">
                        <div class="event_timer">
                            <span>LUN A VIE: 9 AM A 7 PM / SAB: 9 AM A 2PM</span>

                        </div>
                        <div class=" ml-auto">
                            <img src="http://glima.com.mx/img/facebook.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Header Content -->

        <div class="header_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="header_content d-flex flex-row align-items-center justify-content-start">

                            <!-- Logo -->
                            <div class="logo_container">
                                <div class="logo">
                                    <a href="#">
                                        <img src="img/logo.png" width="140" height="120">
                                    </a>
                                </div>
                            </div>

                            <!-- Navigation and Search -->
                            <div class="header_nav_container ml-auto">
                                <nav class="main_nav">
                                    <ul>
                                        <li class="active"><a href="#">Inicio</a></li>
                                        <li><a href="#">Contacto</a></li>
                                        <li><a href="/login">Login</a></li>
                                    </ul>
                                </nav>
                            </div>

                            <!-- Hamburger -->

                            <div class="hamburger ml-auto">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="header_search_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="header_search_content d-flex flex-row align-items-center justify-content-end">
                                <form action="#" class="header_search_form">
                                    <input type="search" class="search_input" placeholder="Search" required="required">
                                    <button class="header_search_button d-flex flex-column align-items-center justify-content-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_2" x="0px" y="0px" viewBox="0 0 56.966 56.966" style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve" width="20px" height="20px">
											<path class="search_path" d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" fill="#353535"/>
										</svg>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>

    <!-- Menu -->

    <div class="menu d-flex flex-column align-items-center justify-content-center">
        <div class="menu_content">
            <nav class="menu_nav">
                <ul>
                    <li class="active"><a href="#">Inicio</a></li>
                    <li><a href="#">Contacto</a></li>
                    <li><a href="/login">Login</a></li>
                </ul>
            </nav>
        </div>
        <div class="menu_close"><i class="fa fa-times" aria-hidden="true"></i></div>
    </div>
    @yield('content')


<!-- Footer -->

    <footer class="footer">
        <div class="container">
            <div class="row">

                <!-- Footer Column -->
                <div class="col-lg-4 footer_col">

                    <!-- Logo -->
                    <div class="logo_container">
                        <div class="logo">
                            <a href="#">
                                <span>Lorem ipsum </span>
                                <img src="{{asset('img/logo.png')}}" width="60" height="50">
                            </a>
                        </div>
                        <br>
                        <div class="logo_subtitle">Lorem ipsum dolor sit amet</div>
                    </div>
                    <div class="footer_social">
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Footer Column -->
                <div class="col-lg-4 footer_col">
                    <div class="footer_links">
                        <div class="footer_title">Enlaces</div>
                        <ul>
                            <li class="active">
                                <a href="#">Inicio</a>
                            </li>
                            <li>
                                <a href="#">Nosotros</a>
                            </li>
                            <li>
                                <a href="#">Productos</a>
                            </li>
                            <li>
                                <a href="#">Servicios</a>
                            </li>
                            <li>
                                <a href="#">Contacto</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Footer Column -->
                <div class="col-lg-3 footer_col">
                    <div class="footer_contact">
                        <div class="footer_title">Contacto</div>
                        <ul>
                            <li>
                                <span>dirección: </span>1481 Lorem ipsum dolor sit amet, Cp 93424</li>
                            <li class="footer_contact_phone">
                                <span>Teléfono: </span>
                                <div>
                                    <div> +53 9993 55 33 56</div>
                                    <div> +53 9993 55 33 56</div>
                                </div>
                            </li>
                            <li>
                                <span>Email: </span>yourmail@gmail.com</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <!-- Copyright -->
        <div class="copyright text-center">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;
            <script>document.write(new Date().getFullYear());</script> All rights reserved
            <i class="fa fa-heart-o" aria-hidden="true"></i> by
            <a href="https://colorlib.com" target="_blank">lorem</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </div>

    </footer>





<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/css/styles/bootstrap4/popper.js"></script>
<script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
<script src="/js/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="/js/plugins/easing/easing.js"></script>
<script src="/js/plugins/parallax-js-master/parallax.min.js"></script>
<script src="/js/custom.js"></script>
</body>
</html>