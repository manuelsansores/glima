<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link href="/css/fontawesome/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet" type="text/css">


    {{--<link href="/vendor/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />--}}
    <link href="/vendor/DataTables/extensions/Select/css/select.bootstrap.min.css" rel="stylesheet" />
    <link href="/vendor/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/vendor/DataTables/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="/vendor/DataTables/media/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/vendor/DataTables/extensions/Responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="/css/jquery-confirm.css">
</head>
<body>
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
        <a href="#" class="navbar-brand"><img src="/img/logo.png" width="100" height="80"></a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarMenu">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarMenu">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="catalogoMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        CATALOGOS
                    </a>
                    <div class="dropdown-menu" aria-labelledby="catalogoMenu">
                        <a class="dropdown-item" href="/article">ARTICULOS</a>
                        <a class="dropdown-item" href="/client">CLIENTES</a>
                    </div>
                </li>

                <li class="nav-item">
                    <a href="" class="nav-link">PEDIDOS</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">SEGUIMIENTO</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="adminMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ADMINISTRACION
                    </a>
                    <div class="dropdown-menu" aria-labelledby="adminMenu">
                        <a class="dropdown-item" href="#">USUARIOS</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('SALIR') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </li>
            </ul>
        </div>
        <a href="#" class="navbar-brand">logo 2</a>
    </nav>
    @yield('content')
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/vendor/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="/vendor/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script src="/vendor/DataTables/extensions/Select/js/dataTables.select.min.js"></script>
    <script src="/vendor/DataTables/extensions/Responsive/js/dataTables.responsive.js"></script>
    <script src="/js/jquery-confirm.js"></script>
    <script src="/js/main.js"></script>
    @yield('script')

</body>
</html>