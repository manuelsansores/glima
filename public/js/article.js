if ( $("#article-content").length > 0 ) {
    var editor;
    var url = '/article';

    $(document).ready(function() {
        editor =  $('#article').DataTable( {

            "ajax": "getArticle",
            "columns": [
                {
                    "data": "id",
                    "visible": false,
                },
                { "data": "article" },
                { "data": "pu" },
                {
                    "data": null,
                    "defaultContent":'<button  class="btn btn-primary btn-sm edit"><i class="far fa-edit"></i></button> <button  class="btn btn-danger btn-sm drop"><i class="far fa-trash-alt"></i></button>'
                }

                /*{
                    data: null,
                    bFilter: false,
                    render: function ( data, type, row ) {
                        return '<!-- Button trigger modal --><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Editar</button><!-- Modal --><div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="test2">' + data.proj_name + '</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">...</div><div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><button type="button" class="btn btn-primary">Save changes</button></div></div></div></div>';


                    }}*/
            ],
            responsive: true

        } );


        // Setup - add a text input to each footer cell
        $('#article tfoot th').each( function () {
            var title = $(this).text();
            if(title != ''){
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            }
        } );

        // DataTable
        var table = $('#article').DataTable();
        // Apply the search
        table.columns().every( function () {
            var that = this;

            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );

        $('#article').on('click', '.drop', function () {
            var data = editor.row( $(this).parents('tr') ).data();
            $('#article_id').val(data.id);

            var r = confirm("¿Desea eliminar el registro?");
            if (r == true) {
                deleteArticle();
            }
        });
        $('#article').on('click', '.edit', function () {
            $('#article-content').show();
            $('#btn-save').hide();
            $('#btn-edit').show();
            var data = editor.row( $(this).parents('tr') ).data();
            $('#article_id').val(data.id);
            //set values
            $('#name_article').val(data.article);
            $('#pu').val(data.pu);


        });

    } );
    $('#article-content').hide();

    $("#btn-new").click(function (e) {
        $('#name_article').val('');
        $('#pu').val('');
        $('#btn-edit').hide();
        $('#btn-save').show();
        $('#article-content').show();
    });

    $("#btn-cancel").click(function (e) {
        e.preventDefault();
        $('#article-content').hide();
    });

    $("#btn-save").click(function (e){
        e.preventDefault();
        saveEdit('save');
    });
    $("#btn-edit").click(function (e){
        e.preventDefault();
        saveEdit('edit');
    });
    
    function deleteArticle() {
        var task_id = $('#article_id').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#_token').val()
            }
        })
        $.ajax({

            type: "DELETE",
            url: url  + '/' + task_id,
            success: function (data) {
                editor.ajax.reload();
            },
            error: function (data) {
            }
        });
    }
    
    function saveEdit(type_action) {
        $('#error-article').removeClass('text-danger');
        $('#error-pu').removeClass('text-danger');
        $('#error-article').html('');
        $('#error-pu').html('');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#_token').val()
            }
        })


        var formData = {
            article: $('#name_article').val(),
            pu: $('#pu').val(),
        }
        //used to determine the http verb to use [add=POST], [update=PUT]
        var state = $('#btn-save').val();
        var type = "POST"; //for creating new resource
        var task_id = $('#article_id').val();
        var my_url = url;
        if (type_action == "edit"){
            type = "PUT"; //for updating existing resource
            my_url='';
            my_url = url+'/' + task_id;
        }
        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                $('#article-content').hide();
                editor.ajax.reload();
            },
            error: function (data) {
                var errors = data.responseJSON.errors;
                $('#error-article').html(errors.article);
                $('#error-article').addClass('text-danger');
                $('#error-pu').html(errors.pu);
                $('#error-pu').addClass('text-danger');
            }
        });
    }

}