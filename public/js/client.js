if ( $("#client-content").length > 0 ) {
    var editor;
    var url = '/client';

    $(document).ready(function() {
        editor =  $('#cliente').DataTable( {

            "ajax": "getClient",
            "columns": [
                {
                    "data": "id",
                    "visible": false,
                },
                { "data": "client" },

                {
                    "data": null,
                    "defaultContent":'<button  class="btn btn-primary btn-sm edit"><i class="far fa-edit"></i></button> <button  class="btn btn-danger btn-sm drop"><i class="far fa-trash-alt"></i></button>'
                }],
            responsive: true

        } );


        // Setup - add a text input to each footer cell
        $('#cliente tfoot th').each( function () {
            var title = $(this).text();
            if(title != ''){
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            }
        } );

        // DataTable
        var table = $('#cliente').DataTable();
        // Apply the search
        table.columns().every( function () {
            var that = this;

            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );

        $('#cliente').on('click', '.drop', function () {
            var data = editor.row( $(this).parents('tr') ).data();
            $('#client_id').val(data.id);

            var r = confirm("¿Desea eliminar el registro?");
            if (r == true) {
                deleteCliente();
            }
        });
        $('#cliente').on('click', '.edit', function () {
            $('#client-content').show();
            $('#btn-save').hide();
            $('#btn-edit').show();
            var data = editor.row( $(this).parents('tr') ).data();
            $('#client_id').val(data.id);
            //set values
            $('#name_client').val(data.client);
            $('#pu').val(data.pu);


        });
    });

//save section
    $('#client-content').hide();

    $("#btn-new").click(function (e) {
        $('#name_client').val('');
        $('#pu').val('');
        $('#btn-edit').hide();
        $('#btn-save').show();
        $('#client-content').show();
    });

    $("#btn-cancel").click(function (e) {
        e.preventDefault();
        $('#client-content').hide();
    });

    $("#btn-save").click(function (e){
        e.preventDefault();
        saveEdit('save');
    });
    $("#btn-edit").click(function (e){
        e.preventDefault();
        saveEdit('edit');
    });

    function deleteCliente() {
        var task_id = $('#client_id').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#_token').val()
            }
        })
        $.ajax({

            type: "DELETE",
            url: url  + '/' + task_id,
            success: function (data) {
                editor.ajax.reload();
            },
            error: function (data) {
            }
        });
    }

    function saveEdit(type_action) {
        $('#error-client').removeClass('text-danger');
        $('#error-client').html('');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#_token').val()
            }
        })


        var formData = {
            client: $('#name_client').val()
        }
        //used to determine the http verb to use [add=POST], [update=PUT]
        var state = $('#btn-save').val();
        var type = "POST"; //for creating new resource
        var task_id = $('#client_id').val();
        var my_url = url;
        if (type_action == "edit"){
            type = "PUT"; //for updating existing resource
            my_url='';
            my_url = url+'/' + task_id;
        }
        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                $('#client-content').hide();
                editor.ajax.reload();
            },
            error: function (data) {
                var errors = data.responseJSON.errors;
                $('#error-client').html(errors.client);
                $('#error-client').addClass('text-danger');
                $('#error-pu').html(errors.pu);
                $('#error-pu').addClass('text-danger');
            }
        });
    }
}